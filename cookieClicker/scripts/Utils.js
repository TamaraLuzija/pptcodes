/**
 * Utility functions
 */
export class Utils {
  /**
   * Sets hardcoded highscores to localstorage, remove when we learn how to fetch
   */
  static setDummyHighscores() {
    if (!localStorage.getItem('highscores')) {
      let dummyHighscores = [
        {
          highscore: 50,
          username: 'Mate'
        },
        {
          highscore: 20,
          username: 'Ivo'
        },
        {
          highscore: 30,
          username: 'Ivan'
        }
      ];
      localStorage.setItem('highscores', JSON.stringify(dummyHighscores));
    }
  }

  /**
   * 
   * @param {number} number 
   * @returns number formated to two digits
   */
  static formatTwoDigits(number) {
    return number < 10 ? "0" + number : number;
  }
}