import { APIClient } from './APIClient.js';
import { LocalStorageUtil } from './LocalStorageUtil.js';
import { Utils } from './Utils.js';

/**
 * CookieGame main class that handles all UI actions
 */
class CookieGame {
  cookieCount = 0;
  gameInterval = null;
  gameStarted = false;
  cookie = null;
  btnStart = null;
  cookieCountDisplay = null;
  timer = null;

  constructor(cookie, cookieCountDisplay, btnStart, timer) {
    this.cookie = cookie;
    this.cookieCountDisplay = cookieCountDisplay;
    this.btnStart = btnStart;
    this.timer = timer;

    this.cookie.onclick = () => {
      this.cookieCountDisplay.textContent = ++this.cookieCount;
    };

    this.btnStart.onclick = () => {
      const tenSeconds = 10;

      if (this.GameStarted) {
        resetGame(tenSeconds);
      } else {
        startGame(tenSeconds);
      }
    };
  }

  get CookieCount() { return this.cookieCount; }
  set CookieCount(count) { this.cookieCount = count; }
  get GameInterval() { return this.gameInterval; }
  set GameInterval(interval) { return this.gameInterval = interval; }
  get GameStarted() { return this.gameStarted; }
  set GameStarted(gameStarted) { this.gameStarted = gameStarted; }

  /**
   * Reset game UI
   * @param {number} duration 
   */
  resetGame(duration) {
    if (!this.GameStarted) {
      return
    }

    let timer = duration;
    this.setTime(timer);

    this.GameStarted = false;

    clearInterval(this.GameInterval);
    this.cookieCountDisplay.textContent = 0;
    this.CookieCount = 0;

    this.btnStart.textContent = "START";
  }

  /**
   * Update highscore list by fetching it from the API
   */
  async updateHighScoreList() {
    let highscoreList = document.querySelector('.highscore--list');

    if (!highscoreList) {
      throw new Error('Can not find highscore list!');
    }

    const highscores = await APIClient.fetchHighScores();

    while(highscoreList.firstChild) {
      highscoreList.removeChild(highscoreList.lastChild);
    }

    highscores.forEach(score => {
      let listItem = document.createElement('li');

      listItem.textContent = `${score.username}: ${score.highscore}`;

      highscoreList.appendChild(listItem);
    });
  }

  /**
   * Start the game
   * @param {number} duration 
   */
  startGame(duration) {
    let timer = duration;
    this.setTime(timer);
    this.btnStart.textContent = "RESET";

    this.GameStarted = true;
    this.CookieCount = 0;

    this.cookieCountDisplay.textContent = 0;

    this.GameInterval = setInterval(async () => {
      timer--;
      if (timer < 0) {
        const username = prompt(`Your score was: ${this.CookieCount}. Please enter your username.`);
        timer = duration;

        this.updateLocalHighscore()

        await APIClient.postHighScore(this.CookieCount, username);
        await this.updateHighScoreList();

        this.resetGame(duration);
      }
      this.setTime(timer);
    }, 1000);
  }

  /**
   * Sets the timer display
   * @param {number} timer 
   */
  setTime(timer) {
    const minutes = Utils.formatTwoDigits(parseInt(timer / 60, 10));
    const seconds = Utils.formatTwoDigits(parseInt(timer % 60, 10));
    this.timer.textContent = minutes + ":" + seconds;
  }

  /**
   * Updates localhighscore and alerts the user if a new highscore has been set
   */
  updateLocalHighscore() {
    if (LocalStorageUtil.setHighScore(this.CookieCount)) {
      alert('Congradulations! You beat your personal highscore!')
    }
  }
}

const cookie = document.getElementById("img--cookie");
const cookieCountDisplay = document.querySelector("#click-count");
const btnStart = document.querySelector(".btn--start");
const timer = document.getElementById('timer');

if (!cookie || !cookieCountDisplay || !btnStart || !timer) {
  alert('UI not loaded!')
} else {
  // Remove when we learn fetch
  Utils.setDummyHighscores();

  const cookieGame = new CookieGame(cookie, cookieCountDisplay, btnStart, timer);

  cookieGame.updateHighScoreList();
}
