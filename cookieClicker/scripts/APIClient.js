/**
 * Function for API communication
 */
export class APIClient {
  /**
   * 
   * @returns List of highscores
   */
  static async fetchHighScores() {
    // API CALLS GO HERE
    const highscores = JSON.parse(localStorage.getItem('highscores'));

    return highscores.sort((score1, score2) => score2.highscore - score1.highscore);
  }

  /**
   * 
   * @param {number} highscore 
   * @param {string} username 
   */
  static async postHighScore(highscore, username) {
    // API CALL GOES HERE
    const highscores = await fetchHighScores();

    highscores.push({
      highscore,
      username
    });

    localStorage.setItem('highscores', JSON.stringify(highscores));
  }
}