/**
 * Util functions for localstorage actions
 */
export class LocalStorageUtil {
  /**
   * 
   * @returns current highscore, throws error if highscore is not set
   */
  static getHighScore() {
    const json = localStorage.getItem('highscore');

    if (!json) {
      throw new Error('Highscore not set!');
    }

    return JSON.parse(json);
  }

  /**
   * 
   * @param {number} highScore 
   * @returns true if new highscore is set, false otherwise
   */
  static setHighScore(highScore) {
    try {
      const currentHighScore = LocalStorageUtil.getHighScore();

      if (currentHighScore.highScore > highScore) {
        return false
      }
    } catch(e) {
      console.log(e);
    }

    const highScoreEntry = {
      date: Date.now(),
      highScore
    };

    localStorage.setItem('highscore', JSON.stringify(highScoreEntry));

    return true;
  }
}