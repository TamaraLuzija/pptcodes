function one() {
  return 1;
}

async function two() {
  return Promise.resolve(2);
}

function three() {
  return 3;
}

console.log(one());
console.log(two());
console.log(three());

console.log(one());
console.log(await two());
console.log(three());
