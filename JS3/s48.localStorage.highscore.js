function getHighScore() {​
  const entry = localstorage.getItem("highscore")​;
  if (entry) {​
    return JSON.parse(entry);​
  }​
  throw new Error('Highscore not set');​
}​

function setHighScore(highScore) {​
  const entry = {​
    highScore: highScore,​
    createdAt: Date.now()​
  };  ​
  localstorage.setItem("highscore", JSON.stringify(entry));​
}​

setHighScore(20);
console.log(getHighScore());